# defines all configuration options for changing stuff about the VMs
@config = {
    # This option is a dictionary providing the different libc versions you can use.
    # Since libc versions are deeply ingrained into the whole OS, every libc version corresponds to an OS version.
    # For example ubuntu bionic uses libc 27, debian buster uses libc 28, etc.
    # The key is the name used internally by Vagrant and what you pass as a parameter to `vagrant up`.
    # For the default, `vagrant up 27` - as an example - will bring up the vm based on ubuntu/bionic64 (which contains libc version 27).
    :libc_versions => {
        # the following properties can be set per libc version:
        # :distro => the name of the distro to use
        # :release => the name of the release of the chosen distro
        # :ip [defaults to 10.10.10.#{libc_version}] => the ip address this machine should be reachable
        # :name [defaults to ctf-#{distro}-#{libc_version}] => the name of this machine (will be used as hostname as well)
        # :box [defaults to #{distro}/#{release}64] => the name of the vagrant box this machine is based on
        23 => {
            :distro => "ubuntu",
            :release => "xenial"
            #:ip => 10.10.10.23
            #:name => "ctf-ubuntu-23"
            #:box => "ubuntu/xenial64"
        },
        27 => {
            :distro => "ubuntu",
            :release => "bionic"
        },
        28 => {
            :distro => "debian",
            :release => "buster"
        },
        29 => {
            :distro => "ubuntu",
            :release => "eoan"
        }
    },

    # Folders that are shared between the VM and your host.
    # This maps folder on your host (the key) to a folder on the VM (the value).
    # You want to put folders here, where you will be working on challenges.
    :shared_folders => {
        "/Users/leonardogalli/Code/CTF/shared" => "/home/vagrant/CTF"
    },

    # Number of Cores the VM should have.
    :num_cores => 2,
    # Number of MB of RAM the VM should have.
    :num_mem => 2048,

    # Shell to install and configure.
    # To configure the default .zshrc setup (including alias'), edit the zshrc.j2 file under playbooks/templates/zshrc.j2
    # The default should be good, if you haven't used zsh before.
    # Currently acceptible values are:
    # :default => Default for os
    # :zsh => zsh
    # :ohmyzsh [default] => zsh + oh-my-zsh (recommended)
    :shell => :ohmyzsh,

    # SSH Key to be installed for all users of the vm.
    # This is just more convenient if you are not inside the vagrant directory.
    :ssh_key => "/Users/leonardogalli/.ssh/id_rsa_old.pub",

    # GDB extension to install.
    # Possible options:
    # :pwndbg
    # :gef [default]
    :gdb_ext => :gef,

    # Options for IDA
    :ida => {
        # Whether to install / include IDA specific stuff, such as dbg servers that are launched at boot time.
        :enabled => true
    },

    # List of architectures packages like binutils (for e.g. shellcoding on that arch) should be installed
    :common_archs => ["aarch64", "i686", "mips"],

    # qemu configuration
    :qemu => {
        # whether to install qemu or not
        :enabled => true,
        # architectures for which to install cross compilers, libc, etc. for running binaries under qemu-user
        :archs => ["mips64", "arm", "aarch64"]
    },

    # Networking
    :network => {
        # format string used for generating a default ip address for the machines
        :ip_fmt => "10.10.20.%d"
    }
}

def get_config()
    ret = @config
    ret[:libc_versions].each do |libc_version, options|
        defaults = {
            :ip => ret[:network][:ip_fmt] % [libc_version],
            :name => "ctf-#{options[:distro]}-#{libc_version}",
            :box => "#{options[:distro]}/#{options[:release]}64"
        }
        ret[:libc_versions][libc_version] = defaults.merge(options)
    end
    return ret
end